package ie.cit.oossp.OnlineVoting.domain;

public class VoteResults {
	
	private String firstName, lastName, party, constit;
	private String votePercent; //Only used to hold and pass value to html code when needed

	private int numFirstVotes, pointsTotal;
	
	public VoteResults() {}

	public VoteResults(String firstName, String lastName, String party, String constit, int numFirstVotes, int pointsTotal) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.numFirstVotes = numFirstVotes;
		this.pointsTotal = pointsTotal;
		this.party = party;
		this.constit = constit;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getParty() {
		return party;
	}

	public void setParty(String party) {
		this.party = party;
	}

	public String getConstit() {
		return constit;
	}

	public void setConstit(String constit) {
		this.constit = constit;
	}
	
	public int getNumFirstVotes() {
		return numFirstVotes;
	}

	public void setNumFirstVotes(int numFirstVotes) {
		this.numFirstVotes = numFirstVotes;
	}

	public int getPointsTotal() {
		return pointsTotal;
	}

	public void setPointsTotal(int pointsTotal) {
		this.pointsTotal = pointsTotal;
	}
	
	public String getVotePercent() {
		return votePercent;
	}

	public void setVotePercent(String votePercent) {
		this.votePercent = votePercent;
	}

	@Override
	public String toString() {
		return "VoteResults [firstName=" + firstName + ", lastName=" + lastName + ", party=" + party + ", constit="
				+ constit + ", numFirstVotes=" + numFirstVotes + ", pointsTotal=" + pointsTotal + "]";
	}

	

}
