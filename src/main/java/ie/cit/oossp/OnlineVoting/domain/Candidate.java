package ie.cit.oossp.OnlineVoting.domain;

public class Candidate {

	private String firstName, lastName, Party, Constituency;
	
	public Candidate() {}
	
	public Candidate(String fName, String lName, String partyName, String constituencyName){
		firstName = fName;
		lastName = lName;
		Party = partyName;
		Constituency = constituencyName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String first_Name) {
		this.firstName = first_Name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String last_Name) {
		this.lastName = last_Name;
	}

	public String getParty() {
		return Party;
	}

	public void setParty(String party) {
		Party = party;
	}

	public String getConstituency() {
		return Constituency;
	}

	public void setConstituency(String constituency) {
		Constituency = constituency;
	}

	@Override
	public String toString() {
		return "Candidate [firstName=" + firstName + ", lastName=" + lastName + ", Party=" + Party + ", Constituency="
				+ Constituency + "]";
	}
}
