package ie.cit.oossp.OnlineVoting.domain;

public class VoteTotals {

	private int totalVotes, validVotes, invalidVotes;
	private String constituency;
	
	public VoteTotals() {}
	
	public VoteTotals( String constituency, int totalVotes, int validVotes, int invalidVotes){
		
		this.constituency = constituency;
		this.totalVotes = totalVotes;
		this.validVotes = validVotes;
		this.invalidVotes = invalidVotes;
	}

	public String getConstituency() {
		return constituency;
	}

	public void setConstituency(String constituency) {
		this.constituency = constituency;
	}

	public int getTotalVotes() {
		return totalVotes;
	}

	public void setTotalVotes(int totalVotes) {
		this.totalVotes = totalVotes;
	}

	public int getValidVotes() {
		return validVotes;
	}

	public void setValidVotes(int validVotes) {
		this.validVotes = validVotes;
	}

	public int getInvalidVotes() {
		return invalidVotes;
	}

	public void setInvalidVotes(int invalidVotes) {
		this.invalidVotes = invalidVotes;
	}

	@Override
	public String toString() {
		return "VoteTotals [totalVotes=" + totalVotes + ", validVotes=" + validVotes + ", invalidVotes=" + invalidVotes
				+ ", constituency=" + constituency + "]";
	}

}
