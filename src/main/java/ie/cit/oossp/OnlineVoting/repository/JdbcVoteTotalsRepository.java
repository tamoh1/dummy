package ie.cit.oossp.OnlineVoting.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcVoteTotalsRepository implements VoteTotalsRepository {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JdbcVoteTotalsRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public void insertValidVote( String constituency) {
		String sql = "INSERT INTO voteTotals (Constituency, total_votes, votes_valid, votes_invalid ) "
				+ "VALUES (?, ?, ?, ?)";
		jdbcTemplate.update(sql, new Object[] { constituency, 1, 1, 0} );
	}

	@Override
	public void insertInvalidVote(String constituency) {
		String sql = "INSERT INTO voteTotals (Constituency, total_votes, votes_valid, votes_invalid ) "
				+ "VALUES (?, ?, ?, ?)";
		jdbcTemplate.update(sql, new Object[] { constituency, 1, 0, 1} );
	}
	
	@Override
	public void updateValidVote(String constituency) {
		String sql = "UPDATE voteTotals SET total_votes = total_votes+1, votes_valid = votes_valid+1 "
				+ "WHERE Constituency = ? ";
		jdbcTemplate.update(sql, new Object[] { constituency} );
	}

	@Override
	public void updateInvalidVote(String constituency) {
		String sql = "UPDATE voteTotals SET total_votes = total_votes+1, votes_invalid = votes_invalid+1"
				+ "WHERE Constituency = ? ";
		jdbcTemplate.update(sql, new Object[] { constituency} );
	}
	
	@Override
	public int checkForExistingConstituency(String constituency) {
		String sql = "SELECT COUNT(*) FROM voteTotals WHERE Constituency = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] { constituency }, Integer.class);
	}

	@Override
	public int getVoteTotal(String constituency) {
		String sql = "SELECT total_votes FROM voteTotals WHERE Constituency = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {constituency}, Integer.class);
	}

	@Override
	public int getValidVoteTotal(String constituency) {
		String sql = "SELECT votes_valid FROM voteTotals WHERE Constituency = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {constituency}, Integer.class);
	}

	@Override
	public int getInvalidVoteTotal(String constituency) {
		String sql = "SELECT votes_invalid FROM voteTotals WHERE Constituency = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {constituency}, Integer.class);
	}

}
