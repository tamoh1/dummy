package ie.cit.oossp.OnlineVoting.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ie.cit.oossp.OnlineVoting.domain.Candidate;
import ie.cit.oossp.OnlineVoting.domain.VoteResults;
import ie.cit.oossp.OnlineVoting.rowmapper.CandidateRowMapper;
import ie.cit.oossp.OnlineVoting.rowmapper.VoteResultsRowMapper;

@Repository
public class JdbcVoteResultsRepository implements VoteResultsRepository {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JdbcVoteResultsRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public void updateVoteResults(VoteResults vResult) {
		if(vResult.getNumFirstVotes() == 1)
		{
			String sql = "UPDATE voteResults SET numFirstVotes = numFirstVotes+1, "
					+ "votePointTotal = votePointTotal+? WHERE First_name = ? AND Second_name = ?";
			jdbcTemplate.update(sql, new Object[] { vResult.getPointsTotal(),
					vResult.getFirstName(), vResult.getLastName()} );
		}
		else
		{
			String sql = "UPDATE voteResults SET votePointTotal = votePointTotal+? "
					+ "WHERE First_name = ? AND Second_name = ?";
			jdbcTemplate.update(sql, new Object[] { vResult.getPointsTotal(),
					vResult.getFirstName(), vResult.getLastName()} );
		}
	}
	
	@Override
	public void insertVoteResults(VoteResults vResult) {
		if(vResult.getNumFirstVotes() == 1)
		{
			String sql = "INSERT INTO voteResults (First_name, Second_name, Party, Constituency, numFirstVotes, votePointTotal ) "
					+ "VALUES (?, ?, ?, ?, ?, ?)";
			jdbcTemplate.update(sql, 
				new Object[] { vResult.getFirstName(), vResult.getLastName(), vResult.getParty(), vResult.getConstit(),
						vResult.getNumFirstVotes(), vResult.getPointsTotal()} );
		}
		else
		{
			String sql = "INSERT INTO voteResults (First_name, Second_name, Party, Constituency, numFirstVotes, votePointTotal ) "
					+ "VALUES (?, ?, ?, ?, ?, ?)";
			jdbcTemplate.update(sql, 
				new Object[] { vResult.getFirstName(), vResult.getLastName(), vResult.getParty(), vResult.getConstit(),
						vResult.getNumFirstVotes(), vResult.getPointsTotal()} );
		}
	}

	@Override
	public int checkForExistingResult(String firstName, String lastName) {
		String sql = "SELECT COUNT(*) FROM voteResults WHERE First_name = ? AND Second_name = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] { firstName, lastName }, Integer.class);
	}
	
	@Override
	public List<VoteResults> getConstituencyList(String constituency) {
		String sql = "SELECT * FROM voteResults WHERE Constituency = ? ORDER BY votePointTotal DESC";
		return jdbcTemplate.query(sql, new Object[] {constituency},  new VoteResultsRowMapper());
	}
	
	@Override
	public List<VoteResults> getAll() {
		String sql = "SELECT * FROM voteResults";
		return jdbcTemplate.query(sql, new VoteResultsRowMapper());
	}

}
