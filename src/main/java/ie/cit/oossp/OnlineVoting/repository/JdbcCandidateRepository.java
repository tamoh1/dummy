package ie.cit.oossp.OnlineVoting.repository;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ie.cit.oossp.OnlineVoting.domain.Candidate;
import ie.cit.oossp.OnlineVoting.rowmapper.CandidateRowMapper;

@Repository
public class JdbcCandidateRepository implements CandidateRepository{
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JdbcCandidateRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public Candidate getCandidate(String firstName, String lastName) {
		String sql = "SELECT * FROM candidates WHERE First_name = ? AND Second_name = ?";
		Candidate candidate = jdbcTemplate.queryForObject(sql, new Object[] { firstName, lastName },
			new CandidateRowMapper());
		return candidate;
	}
	
	@Override
	public String [] getAllCandidates() {
		String sql = "SELECT * FROM candidates";
		List<Map<String, Object>> resultSet = jdbcTemplate.queryForList(sql);
		String[] candidates = new String [resultSet.size()];
		int x = 0;
		for (Map<String, Object> row : resultSet)
		{
			candidates [x] = ((String) row.get("First_name")+" "+(String) row.get("Second_name"));
			x++;
		}
		return candidates;
	}
	
	@Override
	public String [] getAllConstituencies() {
		String sql = "SELECT DISTINCT Constituency FROM candidates";
		List<Map<String, Object>> resultSet = jdbcTemplate.queryForList(sql);
		String[] constituency = new String [resultSet.size()];
		int x = 0;
		for (Map<String, Object> row : resultSet)
		{
			constituency [x] = ((String) row.get("Constituency"));
			x++;
		}
		return constituency;
	}
	
	@Override
	public String [] getCandidatesForConstituency(String constituencyName) {
		String sql = "SELECT * FROM candidates WHERE Constituency = ?";
		List<Map<String, Object>> resultSet = jdbcTemplate.queryForList(sql, constituencyName);
		String[] candidates = new String [resultSet.size()];
		int x = 0;
		for (Map<String, Object> row : resultSet)
		{
			candidates [x] = ((String) row.get("First_name")+" "+(String) row.get("Second_name"));
			x++;
		}
		return candidates;
	}
	
	@Override
	public String getCandidateParty(String firstName, String lastName) {
		String sql = "SELECT Party FROM candidates WHERE First_name = ? AND Second_name = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] { firstName, lastName }, String.class);
	}
	
	@Override
	public List<Candidate> getCandidatesForConstituency(String constituencyName, String dummy) {
		String sql = "SELECT * FROM candidates WHERE Constituency = ?";
		return jdbcTemplate.query(sql, new Object[] { constituencyName }, new CandidateRowMapper());
	}

	@Override
	public List<Candidate> getAll() {
		String sql = "SELECT * FROM candidates";
		return jdbcTemplate.query(sql, new CandidateRowMapper());
	}
}
