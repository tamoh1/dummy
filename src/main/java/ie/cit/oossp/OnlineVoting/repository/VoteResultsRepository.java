package ie.cit.oossp.OnlineVoting.repository;

import java.util.List;

import ie.cit.oossp.OnlineVoting.domain.Candidate;
import ie.cit.oossp.OnlineVoting.domain.VoteResults;

public interface VoteResultsRepository {
	
	public void updateVoteResults(VoteResults vResult);
	
	public void insertVoteResults(VoteResults vResult);
	
	public int checkForExistingResult(String firstName, String lastName);
	
	public List<VoteResults> getConstituencyList(String constituency);
	
	public List<VoteResults> getAll();

}
