package ie.cit.oossp.OnlineVoting.repository;

import java.util.List;

import ie.cit.oossp.OnlineVoting.domain.Candidate;

public interface CandidateRepository {
	
	public Candidate getCandidate(String firstName, String lastName);
	
	public String[] getAllCandidates();
	
	public String[] getAllConstituencies();
	
	public String[] getCandidatesForConstituency(String constituencyName); 
	
	public String getCandidateParty(String firstName, String lastName);
	
	public List<Candidate> getCandidatesForConstituency(String constituencyName, String dummy); 
	
	public List<Candidate> getAll();
	
}