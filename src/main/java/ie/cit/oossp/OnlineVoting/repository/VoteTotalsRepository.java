package ie.cit.oossp.OnlineVoting.repository;

public interface VoteTotalsRepository {
	
	public void insertValidVote(String constituency);
	
	public void insertInvalidVote(String constituency);
	
	public void updateValidVote(String constituency);
	
	public void updateInvalidVote(String constituency);
	
	public int checkForExistingConstituency(String constituency);
	
	public int getVoteTotal(String constituency);
	
	public int getValidVoteTotal(String constituency);
	
	public int getInvalidVoteTotal(String constituency);

}
