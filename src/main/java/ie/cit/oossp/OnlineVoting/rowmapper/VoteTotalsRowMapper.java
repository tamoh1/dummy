package ie.cit.oossp.OnlineVoting.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ie.cit.oossp.OnlineVoting.domain.Candidate;
import ie.cit.oossp.OnlineVoting.domain.VoteTotals;

public class VoteTotalsRowMapper implements RowMapper<VoteTotals> {

	@Override
	public VoteTotals mapRow(ResultSet rs, int index) throws SQLException {
		VoteTotals voteTotals = new VoteTotals();
	
		voteTotals.setConstituency(rs.getString("Constituency"));
		voteTotals.setTotalVotes(rs.getInt("total_votes"));
		voteTotals.setValidVotes(rs.getInt("votes_valid"));
		voteTotals.setInvalidVotes(rs.getInt("votes_invalid"));
		
		return voteTotals;
	}

}
