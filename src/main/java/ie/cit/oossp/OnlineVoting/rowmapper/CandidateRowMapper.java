package ie.cit.oossp.OnlineVoting.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ie.cit.oossp.OnlineVoting.domain.Candidate;

public class CandidateRowMapper implements RowMapper<Candidate> {

	@Override
	public Candidate mapRow(ResultSet rs, int index) throws SQLException {
		Candidate candidate = new Candidate();
	
		candidate.setFirstName(rs.getString("First_name"));
		candidate.setLastName(rs.getString("Second_name"));
		candidate.setParty(rs.getString("Party"));
		candidate.setConstituency(rs.getString("Constituency"));
		
		return candidate;
	}
}