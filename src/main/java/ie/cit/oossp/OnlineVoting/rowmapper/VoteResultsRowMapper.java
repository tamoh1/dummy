package ie.cit.oossp.OnlineVoting.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ie.cit.oossp.OnlineVoting.domain.Candidate;
import ie.cit.oossp.OnlineVoting.domain.VoteResults;

public class VoteResultsRowMapper implements RowMapper<VoteResults> {

	@Override
	public VoteResults mapRow(ResultSet rs, int index) throws SQLException {
		
		VoteResults voteResults = new VoteResults();
		
		voteResults.setFirstName(rs.getString("First_name"));
		voteResults.setLastName(rs.getString("Second_name"));
		voteResults.setParty(rs.getString("Party"));
		voteResults.setConstit(rs.getString("Constituency"));
		voteResults.setNumFirstVotes(rs.getInt("numFirstVotes"));
		voteResults.setPointsTotal(rs.getInt("votePointTotal"));
		
		return voteResults;
	}
}
