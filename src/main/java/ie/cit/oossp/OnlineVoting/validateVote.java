package ie.cit.oossp.OnlineVoting;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Base64;

import javax.servlet.http.Cookie;

/*
 * Class is used to ensure that the vote entered is valid.
 */
public class validateVote implements Serializable{
	
	int counter;
	
	public validateVote() {}
	
	//This method ensures voting is in order and no errors with the voting
	public int validateVoteDetails(ArrayList<String> voteList)
	{	
		//No empty votes
		if( voteList.indexOf("") == -1 )
		{
			//Checks list for duplicates
			counter = 1;
			for(int i = 0; i < voteList.size(); i++)
			{
				for(int k = counter; k < voteList.size(); k++)
				{
					if(voteList.get(i).toString().equalsIgnoreCase(voteList.get(k).toString()))
					{
						return 1;
					}
				}
				counter++;
			}
			return 0;
		}
		//At least one empty vote
		else
		{
			//If the first vote is empty  return 1 for error
			if(voteList.get(0).toString().equalsIgnoreCase(""))
			{
				return 1;
			}
			else
			{
				//Loop through votes and check for a gap 
				boolean gap = false;
				for(int i = 0;i < voteList.size()-1;i++)
				{
					if(voteList.get(i).toString().equalsIgnoreCase("") && !voteList.get(i+1).toString().equalsIgnoreCase("") )
					{
						gap = true;
						return 1;
					}
				}
				//If there is no gap in the votes
				if( gap == false )
				{
					counter = 1;
					for(int i = 0; i < voteList.size(); i++)
					{
						for(int k = counter; k < voteList.size(); k++)
						{
							//If the current value equals the next value of list then if statement is actioned
							if(voteList.get(i).toString().equalsIgnoreCase(voteList.get(k).toString()))
							{
								//If the current value equals an empty string then do nothing
								if(voteList.get(i).toString().equalsIgnoreCase(""))
								{}
								else  //If not an empty string issue vote error
								{ return 1; }
							}
						}
						counter++;
					}
				}
			}
		}
		return 0;		
	}
}

