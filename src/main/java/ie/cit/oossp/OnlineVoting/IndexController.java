package ie.cit.oossp.OnlineVoting;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import groovyjarjarasm.asm.commons.Method;
import ie.cit.oossp.OnlineVoting.domain.VoteResults;
import ie.cit.oossp.OnlineVoting.service.CandidateService;
import ie.cit.oossp.OnlineVoting.service.VoteResultsService;
import ie.cit.oossp.OnlineVoting.service.VoteTotalsService;

@Controller
public class IndexController {
	
	@Autowired 
	CandidateService candidateService;
	
	@Autowired 
	VoteTotalsService voteTotalsService;
	
	@Autowired 
	VoteResultsService voteResultsService;
	
	DecimalFormat df = new DecimalFormat("#.##");
	
	@RequestMapping(value="/", method=RequestMethod.GET)
    public String greeting( Model model) {
		
		String[] names = candidateService.getAllConstituencies();
        
        model.addAttribute("names", names);
        return "index";
    }
	/*
	 * Checks if entry picked and directs to proper set up details
	 */
	@RequestMapping(value="/vote", method=RequestMethod.GET)
    public String greeting1( HttpServletRequest request, Model model) {
        
		String area = request.getParameter("preference");
		String choice = request.getParameter("resultsBtn");
		
		if( area.isEmpty() )
		{
			return "invalidvote";
		}
		else if ( choice != null && area != null )
		{
			this.greeting3(request, model, area);
			return "finalresults";
		}
		else
		{
			String[] names = candidateService.getCandidatesForConstituency(area);
		    model.addAttribute("names", names);
		    model.addAttribute("constituency",area);
		    return "vote";
		}
    }
	
	/*
	 * Used when vote is done and directs to good or bad vote
	 * Updates applicable tables depending on whats chosen
	 */
	@RequestMapping(value="/validvote", method=RequestMethod.GET)
    public String greeting2( HttpServletRequest request, Model model) {
        
		String area = request.getParameter("constituency");
		//Holds list selected by user and set up through looping parameters
		ArrayList<String> prefList = new ArrayList();	
		int listTotal = 6;
		for(int i = 1; i<listTotal; i++)
		{
			prefList.add(request.getParameter("pref"+i));
		}
		
		//Following section sets up valid vote type objects
		ArrayList<VoteResults> resultList = new ArrayList();
		VoteResults vr = null;
		String[] splited = null;
		String fName = "";
		String lName = "";
		String party = "";
		int points = 0;
		boolean firstChoice = false;
		for(int i = 0; i < prefList.size(); i++)
		{
			if(prefList.get(i).toString().isEmpty())
			{
			}
			else
			{
				splited = prefList.get(i).split(" ", 2);
				fName = splited[0];
				lName = splited[1];
				party = candidateService.getCandidateParty(fName, lName);
				points = prefList.size()-i;
				if(i == 0)
				{
					vr = new VoteResults(fName, lName, party, area, 1, points);
				}
				else
				{
					vr = new VoteResults(fName, lName, party, area, 0, points);
				}
				resultList.add(vr);
			}	
		}
		
		validateVote vb = new validateVote(); //Validates whether proper vote order was done
		boolean invalidVote = false;
		int test = 2;
		
		test = vb.validateVoteDetails(prefList);	
		
		//If not valid vote
		if(test > 0)
		{	
			int checker2 = 0;	
			checker2 = voteTotalsService.checkForExistingConstituency(area);
			
			if(checker2 == 1)
			{
				voteTotalsService.updateInvalidVote(area);
			}
			else
			{
				voteTotalsService.insertInvalidVote(area);
			}
		
			String totalVotes = "Total votes "+voteTotalsService.getVoteTotal(area)+" registered for "+area+" to date";
			model.addAttribute("total", totalVotes);
			
			return "invalidvote";
		}
		//If vote is valid
		else
		{
			int checker = 0;
			int checker2 = 0;
			checker2 = voteTotalsService.checkForExistingConstituency(area);
			
			if(checker2 == 1)
			{
				voteTotalsService.updateValidVote(area);
			}
			else
			{
				voteTotalsService.insertValidVote(area);
			}
			
			for(int i = 0; i < resultList.size(); i++)
			{
				checker = voteResultsService.checkForExistingResult(resultList.get(i).getFirstName(), resultList.get(i).getLastName() );
				if(checker == 1)
				{
					voteResultsService.updateVoteResults(resultList.get(i));
				}
				else
				{
					voteResultsService.insertVoteResults(resultList.get(i));
				}
			}	
			String totalVotes = "Total of "+voteTotalsService.getVoteTotal(area)+" votes registered for "+area+" to date";
			model.addAttribute("total", totalVotes);
			model.addAttribute("currVote", resultList);
			
			return "validvote";
		}	
    }
	
	/*
	 * Set up variables for results page
	 *  Displays results for only 1 constituency at a time 
	 */
	@RequestMapping(value="/finalresults", method=RequestMethod.GET)
    public String greeting3( HttpServletRequest request, Model model, String area) {
		
		List<VoteResults> resList = voteResultsService.getConstituencyList(area);
		//Following is used to calculate % of 1st votes and add to objects before
		//being passed to model and used in html
		int totalValidVotes = voteTotalsService.getValidVoteTotal(area);
		double d = 0;
		for(int i = 0;i<resList.size();i++)
		{
			d =  (double) resList.get(i).getNumFirstVotes()/totalValidVotes;
			resList.get(i).setVotePercent(df.format(d*100)+"%");
		}
		
		model.addAttribute("area", area);
		model.addAttribute("names", resList);
	
		return "finalresults";
	}
}