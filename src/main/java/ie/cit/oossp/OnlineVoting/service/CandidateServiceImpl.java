package ie.cit.oossp.OnlineVoting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ie.cit.oossp.OnlineVoting.domain.Candidate;
import ie.cit.oossp.OnlineVoting.repository.CandidateRepository;

@Service
public class CandidateServiceImpl implements CandidateService {

	CandidateRepository candidateRepository;
	
	@Autowired
	public CandidateServiceImpl(CandidateRepository candidateRepository) {
		this.candidateRepository = candidateRepository;
	}

	@Override
	public Candidate getCandidate(String firstName, String lastName) {
		
		return candidateRepository.getCandidate(firstName, lastName);
	}

	@Override
	public String[] getAllCandidates() {
		
		return candidateRepository.getAllCandidates();
	}

	@Override
	public String[] getAllConstituencies() {
		
		return candidateRepository.getAllConstituencies();
	}

	@Override
	public String [] getCandidatesForConstituency(String constituencyName) {
		
		return candidateRepository.getCandidatesForConstituency(constituencyName);
	}
	
	@Override
	public String getCandidateParty(String firstName, String lastName){
		
		return candidateRepository.getCandidateParty(firstName, lastName);
	}
	
	@Override
	public List<Candidate> getCandidatesForConstituency(String constituencyName, String dummy) {
		
		return candidateRepository.getCandidatesForConstituency(constituencyName, dummy);
	}

	@Override
	public List<Candidate> getAll() {
		
		return candidateRepository.getAll();
	}
}