package ie.cit.oossp.OnlineVoting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ie.cit.oossp.OnlineVoting.domain.VoteResults;
import ie.cit.oossp.OnlineVoting.repository.VoteResultsRepository;

@Service
public class VoteResultsServiceImpl implements VoteResultsService {

	VoteResultsRepository voteResultsRepository;
	
	@Autowired
	public VoteResultsServiceImpl(VoteResultsRepository voteResultsRepository) {
		this.voteResultsRepository = voteResultsRepository;
	}
	
	@Override
	public void updateVoteResults(VoteResults vResult) {
		voteResultsRepository.updateVoteResults(vResult);
		
	}

	@Override
	public void insertVoteResults(VoteResults vResult) {
		voteResultsRepository.insertVoteResults(vResult);
		
	}

	@Override
	public int checkForExistingResult(String firstName, String lastName) {
		return voteResultsRepository.checkForExistingResult(firstName, lastName);
	}

	@Override
	public List<VoteResults> getConstituencyList(String constituency){
		return voteResultsRepository.getConstituencyList(constituency);
	}
	
	@Override
	public List<VoteResults> getAll() {
		
		return voteResultsRepository.getAll();
	}

}
