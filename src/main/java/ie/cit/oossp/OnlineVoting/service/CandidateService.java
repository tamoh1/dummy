package ie.cit.oossp.OnlineVoting.service;

import java.util.List;

import ie.cit.oossp.OnlineVoting.domain.Candidate;

public interface CandidateService {

	public Candidate getCandidate(String firstName, String lastName);
	
	public String[] getAllCandidates();
	
	public String[] getAllConstituencies();
	
	public String[] getCandidatesForConstituency(String constituencyName); 
	
	public String getCandidateParty(String firstName, String lastName);
	
	public List<Candidate> getCandidatesForConstituency(String constituencyName, String dummy); 
	
	public List<Candidate> getAll();
}