package ie.cit.oossp.OnlineVoting.service;

import java.util.List;

import ie.cit.oossp.OnlineVoting.domain.VoteResults;

public interface VoteResultsService {
	
	public void updateVoteResults(VoteResults vResult);
	
	public void insertVoteResults(VoteResults vResult);
	
	public int checkForExistingResult(String firstName, String lastName);
	
	public List<VoteResults> getConstituencyList(String constituency);
	
	public List<VoteResults> getAll();

}
