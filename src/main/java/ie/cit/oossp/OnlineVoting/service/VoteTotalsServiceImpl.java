package ie.cit.oossp.OnlineVoting.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ie.cit.oossp.OnlineVoting.repository.VoteTotalsRepository;

@Service
public class VoteTotalsServiceImpl implements VoteTotalsService {

	VoteTotalsRepository voteTotalsRepository;
	
	@Autowired
	public VoteTotalsServiceImpl(VoteTotalsRepository voteTotalsRepository) {
		this.voteTotalsRepository = voteTotalsRepository;
	}
	
	@Override
	public void insertValidVote(String constituency) {
		voteTotalsRepository.insertValidVote(constituency);
	}

	@Override
	public void insertInvalidVote(String constituency) {
		voteTotalsRepository.insertInvalidVote(constituency);
	}
	
	@Override
	public void updateValidVote(String constituency) {
		voteTotalsRepository.updateValidVote(constituency);
	}

	@Override
	public void updateInvalidVote(String constituency) {
		voteTotalsRepository.updateInvalidVote(constituency);
	}
	
	public int checkForExistingConstituency(String constituency){
		return voteTotalsRepository.checkForExistingConstituency(constituency);
	}

	@Override
	public int getVoteTotal(String constituency) {
		
		return voteTotalsRepository.getVoteTotal(constituency);
	}

	@Override
	public int getValidVoteTotal(String constituency) {
		
		return voteTotalsRepository.getValidVoteTotal(constituency);
	}

	@Override
	public int getInvalidVoteTotal(String constituency) {
		return voteTotalsRepository.getInvalidVoteTotal(constituency);
	}

}
