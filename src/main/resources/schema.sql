CREATE TABLE candidates(
  First_name VARCHAR(13),
  Second_name VARCHAR(20),
  Party VARCHAR(29),
  Constituency VARCHAR(20)
);


CREATE TABLE voteTotals(
  Constituency VARCHAR(30),
  total_votes INT(11),
  votes_valid INT(11),
  votes_invalid INT(11)
);


CREATE TABLE voteResults(
  First_name VARCHAR(13),
  Second_name VARCHAR(20),
  Party VARCHAR(30),
  Constituency VARCHAR(30),
  numFirstVotes INT(11),
  votePointTotal INT(11)
);

/*
CREATE TABLE movements ( 
  id int(11) NOT NULL AUTO_INCREMENT, 
  name varchar(100) NOT NULL, 
  PRIMARY KEY (id) 
);
*/
