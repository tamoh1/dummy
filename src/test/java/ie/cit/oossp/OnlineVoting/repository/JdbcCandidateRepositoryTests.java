package ie.cit.oossp.OnlineVoting.repository;

import static org.junit.Assert.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ie.cit.oossp.OnlineVoting.OnlineVotingApplication;
import ie.cit.oossp.OnlineVoting.domain.Candidate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OnlineVotingApplication.class)
@ActiveProfiles("test")
@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class JdbcCandidateRepositoryTests{
	
	@Autowired
	JdbcCandidateRepository repo;
	
	@Test
	public void getCandidate() {
		Candidate c = repo.getCandidate("John", "Joseph");
		assertEquals("John, Joseph", c.getFirstName()+", "+c.getLastName());
	}
	
	@Test
	public void getAllCandidates() {
		String[] candidates = repo.getAllCandidates();
		assertEquals(75, candidates.length);
	}
	
	@Test
	public void getAllConstituencies() {
		String[] constituencies = repo.getAllConstituencies();
		assertEquals(20, constituencies.length);
	}
	
	@Test
	public void getCandidatesForConstituency() {
		String[] candidates = repo.getCandidatesForConstituency("Cork");
		assertEquals(9, candidates.length);
	}
	
	@Test
	public void getCandidateParty() {
		String party = repo.getCandidateParty("John", "Joseph");
		assertEquals(party, "Cork");
	}
	
	@Test
	public void getCandidatesForConstituency(String constituencyName, String dummy) {
		List<Candidate> list = repo.getCandidatesForConstituency("Cork", "");
		assertEquals(9, list.size());
	}

	@Test
	public void getAll() {
		List<Candidate> list = repo.getAll();
		assertEquals(75, list.size());
	}
}
