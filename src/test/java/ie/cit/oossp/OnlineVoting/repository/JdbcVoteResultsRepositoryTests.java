package ie.cit.oossp.OnlineVoting.repository;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ie.cit.oossp.OnlineVoting.OnlineVotingApplication;
import ie.cit.oossp.OnlineVoting.domain.VoteResults;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OnlineVotingApplication.class)
@ActiveProfiles("test")
@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class JdbcVoteResultsRepositoryTests{

	@Autowired
	JdbcVoteResultsRepository repo;
	
	@Test
	public void updateVoteResults() {
		List<VoteResults> list = repo.getAll();
		assertEquals(5, list.size());
	}
	
	@Test
	public void insertVoteResults() {
		List<VoteResults> list = repo.getAll();
		assertEquals(1, list.size());
	}

	@Test
	public void checkForExistingResult() {
		int i = repo.checkForExistingResult("John", "Joseph");
		assertEquals(1, i);
	}
	
	@Test
	public void getConstituencyList() {
		List<VoteResults> list = repo.getConstituencyList("Cork");
		assertEquals(9, list.size());
	}
	
	@Test
	public void getAll() {
		List<VoteResults> list = repo.getAll();
		assertEquals(1, list.size());
	}

}
