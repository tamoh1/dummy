package ie.cit.oossp.OnlineVoting.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ie.cit.oossp.OnlineVoting.OnlineVotingApplication;
import ie.cit.oossp.OnlineVoting.domain.VoteResults;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = OnlineVotingApplication.class)
@ActiveProfiles("test")
@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class JdbcVoteTotalsRepositoryTests {

	@Autowired
	JdbcVoteTotalsRepository repo;
	
	@Test
	public void insertValidVote() {
		int num = repo.getValidVoteTotal("Cork");
		assertEquals(1, num);
	}

	@Test
	public void insertInvalidVote() {
		int num = repo.getInvalidVoteTotal("Cork");
		assertEquals(1, num);
	}
	
	@Test
	public void updateValidVote() {
		int num = repo.getValidVoteTotal("Cork");
		assertEquals(1, num);
	}

	@Test
	public void updateInvalidVote() {
		int num = repo.getInvalidVoteTotal("Cork");
		assertEquals(1, num);
	}
	
	@Test
	public void checkForExistingConstituency() {
		int i = repo.checkForExistingConstituency("Cork");
		assertEquals(1, i);
	}

	@Test
	public void getVoteTotal() {
		int num = repo.getVoteTotal("Cork");
		assertEquals(0, num);
	}

	@Test
	public void getValidVoteTotal() {
		int num = repo.getValidVoteTotal("Cork");
		assertEquals(0, num);
	}

	@Test
	public void getInvalidVoteTotal() {
		int num = repo.getInvalidVoteTotal("Cork");
		assertEquals(0, num);
	}
}
